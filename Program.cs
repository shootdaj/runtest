﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Threading;

namespace RunTest
{
	/// <summary>
	/// A wrapper for the bundle console application to run Ruby/Cucumber tests
	/// </summary>
	class Program
	{
		static void Main(string[] args)
		{
			string storyName = "";

			//if no story name was provided in command line, get it from user -- "exit", "quit" or "x" input at this step exits the process
			if (args.Length == 0)
			{
				while (storyName == string.Empty)
				{
					Console.Write("Please provide a story name (ex. story_48716_2) : ");
					string input = Console.ReadLine();
					if (input == "exit" || input == "quit" || input == "x")
						Environment.Exit(0);
					else
						storyName = input;
				}
			}
			else
				storyName = args[0];

			//if no environment and/or profile was provided in the command line,
			//give the user the opportunity to override the default environment and profile
			//if no input is received within a given time, use the default values in app.config
			//this timeout value itself can be set in app.config with key "InputTimeout" and units of milliseconds
			string environment = args.Length > 1
				                     ? args[1]
				                     : Reader.ReadLine(int.Parse(GetConfigValue("InputTimeout")), out environment,
				                                       "Override default test environment (leave blank for default) : ") &&
				                       !string.IsNullOrEmpty(environment)
					                       ? environment
					                       : GetConfigValue("Environment");
			Console.WriteLine("");
			string profile = args.Length > 1
				                 ? args[2]
				                 : Reader.ReadLine(int.Parse(GetConfigValue("InputTimeout")), out profile,
				                                   "Override default test profile (leave blank for default) : ") &&
				                   !string.IsNullOrEmpty(profile)
					                   ? profile
					                   : ConfigurationManager.AppSettings["Profile"];
			Console.WriteLine("");

			//set all process parameters to run the test
			ProcessStartInfo testProcessStartInfo = new ProcessStartInfo();
			testProcessStartInfo.WorkingDirectory = @"C:\TFS\Grange Commercial SEQ\" + environment + @"\Specifications\GainWeb";
			testProcessStartInfo.FileName = @"C:\RailsInstaller\Ruby1.9.3\bin\bundle.bat";
			testProcessStartInfo.UseShellExecute = false;
			testProcessStartInfo.RedirectStandardOutput = true;
			//testProcessStartInfo.CreateNoWindow = false;
			testProcessStartInfo.Arguments = "exec cucumber -p " + profile + " -t @" + storyName;
			
			Process testProcess = new Process();
			testProcess.StartInfo = testProcessStartInfo;

			//start the test and read in the redirected output from the test runner (bundle)
			testProcess.Start();
			testProcess.OutputDataReceived += (sender, e) => Console.WriteLine(e.Data);
			testProcess.BeginOutputReadLine();
			testProcess.WaitForExit();
			Console.WriteLine("");
			Console.WriteLine("Press any key to exit...");
			Console.ReadLine();
		}

		/// <summary>
		/// Gets the configuration value defined by the given key.
		/// </summary>
		/// <param name="key"></param>
		/// <returns></returns>
		private static string GetConfigValue(string key)
		{
			return ConfigurationManager.AppSettings[key];
		}
	}

	/// <summary>
	/// Timeout-based console reader
	/// </summary>
	class Reader
	{
		private static Thread inputThread;
		private static AutoResetEvent getInput, gotInput;
		private static string input;

		static Reader()
		{
			inputThread = new Thread(reader);
			inputThread.IsBackground = true;
			inputThread.Start();
			getInput = new AutoResetEvent(false);
			gotInput = new AutoResetEvent(false);
		}

		private static void reader()
		{
			while (true)
			{
				getInput.WaitOne();
				input = Console.ReadLine();
				gotInput.Set();
			}
		}

		public static bool ReadLine(int timeOutMillisecs, out string userInput, string prompt = "")
		{
			if (!String.IsNullOrEmpty(prompt))
				Console.Write(prompt);
			getInput.Set();
			bool success = gotInput.WaitOne(timeOutMillisecs);
			userInput = input;
			return success;

			//bool success = gotInput.WaitOne(timeOutMillisecs);
			//if (success)
			//    return input;
			//else
			//    throw new TimeoutException("User did not provide input within the timelimit.");
		}
	}
}
